参考：

> 郭炜等 SoC 设计方法与实现(第 3 版)．电子工业出版社.出版时间:2017-08-01.第八章.
> Synopsys Design Compiler User Guide.
> Rakesh ChadhaJ.Bhasker. Static Timing Analysis forNanometer Designs. Springer,2009.Chapter-3.

[芯动力——硬件加速设计方法_西南交通大学](https://www.icourse163.org/course/SWJTU-1207492806?tid=1467145670)

[DC 脚本及解释 - 百度文库](https://wenku.baidu.com/view/1ccdfed3360cba1aa811dac6?aggId=0e96ed61915f804d2b16c19b&fr=catalogMain_text_ernie_recall:wk_recommend_main_graph&_wkts_=1679473272674)

[design compile 介绍 非常好](https://www.skfwe.cn/p/design-compile-%E4%BB%8B%E7%BB%8D/)

逻辑综合 Synthesis，工具是 Design Compiler

# 1 基本概念

- 作用&目的：

  简单来说是行为描述的电路、RTL 级电路转换成门级的过程，具体是将 HDL 语言描述的电路转换到基于工艺库的门级网表（也就是 DC 的功能）。同时决定电路门级结构、寻求**时序**和与**面积**的平衡、寻求**功耗**与时序的平衡、增强电路的测试性。
- 原理&过程：

  对电路的综合包括三个步骤：**翻译，优化，映射**：Synthesis=Translation + Optimization + Mapping

  - Translation 是指把 **HDL** 语言描述的电路转化为用 **GTECH 库元件组成的逻辑电路**的过程。GTECH 是 Synopsys 的**通用工艺库**,它仅表示了逻辑函数的功能,并没有映射到具体的厂家工艺库,也就是说是独立于厂家工艺的。
  - Optimization 是根据设计者对电路设定的**延时和面积等约束条件**对电路进行优化的过程。它通过各种方法尽量满足设计者对电路的要求。
  - Mapping 把用 GTECH 库元件构成的电路映射到某一特定厂家的**工艺库**上,此时的电路包含了厂家的工艺参数 **Library cells**。

  首先，综合工具分析 HDL 代码，用一种模型(GTECH 库是 Synopsys 公司提供的通用的、独立于工艺的元件库) ，对 HDL 进行映射，这个模型是与技术库无关的；然后，在设计者的控制下，对这个模型进行逻辑优化；最后一步，进行逻辑映射和门级优化，将逻辑根据约束，映射为专门的技术目标单元库（target cell library）中的 cell，形成了综合后的网表。
- Synthesis 输入：RTL 代码，工艺库，约束
- Synthesis 输出：Netlist 门级网表（用于布局布线），标准延迟文件（用于时序仿真）；综合后的报告；

# 2 设计对象

![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20240309000333-27jfh2t.png)

- design: 能完成一定功能的逻辑实体，待综合的对象
- port: design 的（最外部）输入输出端口
- clock: design 的输入时钟
- cell: 被调用单元的例化名
- reference: 被调用例化的实例
- pin: 内部调用单元的输入输出管脚
- net: pin 和 pin 之间以及 pin 和 port 之间的连线。

# 3 综合流程

![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%201-20230902111216-k5fjsqj.png)

# 4 库文件

![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%202-20230902111216-9uqmwg7.png)

1. 目标库 `target_library`​

   综合后电路网表要最终映射到的库；对应标准单元
   目标库是由 Foundary 提供的，一般是.db 的格式。.db 格式可以由文本格式的.lib 转化过来，它们包含的信息是一致的。目标库中包含了各个门级单元的行为、引脚、面积、驱动能力以及时序信息(有的工艺库还有功耗方面的参数)
2. 链接库 `link_library`​

   用来设置模块或者单元电路的引用；对应 IP（付费 IP、存储器、IO、PAD 等）
   需要制定搜索路径 `search_path`​，并带星号“*”（表示 DC 在引用实例化模块或者单元电路时首先搜索已经调进 DC memory 的模块和单元电路）
   目标库和链接库可能一样也可能不一样，统称为 Technology library
3. 符号库 `symbol_library`​

   单元电路显示的 Schematic 的库，文件格式 `.sdb`​ ，如果没有设置，DC 会使用默认的
   包含元件的图形符号，在 GUI 界面显示设计的电路图，符号图一般由 Vendor 提供。

   `set symbol_library`​ 设置符号库
4. 算术运算库 `synthetic_library`​ = DesignWare Library
   DesignWare Library 可以完成诸如复杂的算术运算(+,-,*,/等),数值比较(,>5,< 出等),以及 FIFO、CPU 等功能， DC 识别的 DesignWare 是 `.sldb` ​格式的 Library
   DesignWarelibrary 使用变量 synthetic_library 设置，同时在 link_library 中设置。
   standard.sldb 是个特殊的 DesignWare 库，在 HDL translate 时，用于构建 HDL 中的操作符，并且 DC 会自动调用 standard.sldb
   ddw_foundation.sldb 是 synopsys 的 DesignWare 综合库
   可能需要更高级的 license
5. GTECH 库
   GTECH Library 是 Synopsys 的通用工艺库,它由 DesignCompiler 自带,是独立于厂家工艺的。
   该库中包含的元件仅代表一定的逻辑功能而不带有任何工艺参数，因此是工艺无关的。
   在综合过程中， DC 首先将 HDL 代码 Translate 到 GTECH 库,然后在综合(compile)时再 DC 自动调用映射(Map)到工艺库。
   在综合过程中，GTECH 库，不需要任何设置。

# 5 工艺库内部信息

## 5.1 时序影响因素

单元时序模型旨在为设计环境中的标准单元的各种实例提供**精确的时序**。

![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226162634-nohlhkw.png)​

Timing arc 的延迟取决于两个因素:

第一，**输出负载**，即反相器输出引脚的电容负载
第二，输入信号的**转换时间 transition_time**

**负载电容越大，延迟越大。在大多数情况下，延迟随着输入转换时间的增加而增加。** 
确定了输入转换时间和输出电容后，标准单元就可以根据时序模型来查询具体的延迟信息。

## 5.2 查询延迟信息

通常来说，标准单元的时序模型通常有两种，线性模型和非线性模型。大多数标准单元库目前使用更复杂的非线性延迟模型。

在标准单元工艺库中，延迟的非线性时序模型(NLDM)以二维形式呈现，其中两个独立变量 index，是输入转换时间和输出负载电容，这个二维查找表中的内容就是延迟。

![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%203-20230902111216-egivyx2.png)

index_1：输入转换时间；index_2：输出负载电容；INP1 到 OUT 之间的延迟，上升下降延迟分别表示 cell_rise 和 cell_fall。

对于一个标准单元其实除了两个 delay 表，还会有 2 个 transition 表，可以找出输出端口的 transition。

应该还有一个查找表模板指定变量意思。index_1 行索引，另一个列索引，内容是虚拟占位符

![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%204-20230902111216-iqy0trb.png)​

## 5.3 slew 值

slew 值基于库中指定的测量阈值。大多数库（早期版本）使用 10% 和 90% 作为转换的测量阈值。新一代时序库使用 Vdd 的 30% 和 70% 作为测量阈值。

由于转换时间之前的测量值介于 10% 和 90% 之间，因此在填充库时，测量的转换时间通常会增加一倍，介于 30% 和 70% 之间。

使用转换 Derate 系数：通常指定为 0.5

![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226170024-qtf4eoj.png)​

转换时间必须乘以 0.5，以获得与转换值(30-70)设置相对应的转换时间。

这意味着转换表中的值( 以及相应的索引值 )实际上是 10-90 值。

在表征期间，转变在 30-70 处测量，并且库中的转变数据对应于测量值的外推至 10% 至 90%( ( 70-30 ) /( 90-10 ) = 0.5 )。

![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%205-20230902111216-wm24mvb.png)

表中说明我们需要使用的测量阈值是 20 到 80。slew 设置为 0.6，所以库中的转换时间的阈值为 0 到 100。((80-20 )/ (100-0 ) = 0.6)

实际使用的 `transition_time = library_transition_time_value * slew_derate`​5.4 时序模型

## 5.4 时序模型

`cell_rise` ​是指逻辑门输入信号上升沿到输出信号上升沿之间的延迟时间，而 `rise_transition` ​是指逻辑门输出信号上升时间。它们都是标准单元库中的重要时序参数

* 组合逻辑

  ![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226170436-xx7ju0a.png)​

  让我们考虑双输入和单元的时序弧。这个单元的两个时序弧都是 positive_unate；因此输入引脚上升对应于输出上升，反之亦然
  这意味着对于 NLDM 模型，将有四个用于指定延迟的表模型。类似地，将有四个这样的表模型用于指定输出转换时间。

  ![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226170659-zoxoqx5.png)
* 时序逻辑

  时序器件就会有对于上升和下降的 setup 和 hold time

  ![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226170739-4xihdzt.png)​

  ![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226170817-8le6jid.png)​

  ![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226170836-n91mtsq.png)​

  ![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226170938-tm2zuye.png)​​

‍

## 5.5 命名约定

![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226164139-tmlb1ti.png)​

![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226164154-wbkt2tw.png)​

![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226164210-ntreem2.png)​

![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226165553-cjc6jxy.png)​

## 5.6 单元库案例

![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226164918-u2u3bi9.png)​

![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226164927-e46fhwz.png)​

![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226164938-shg45ry.png)​

![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226164951-4595ecx.png)​

![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226164959-ufbq0z8.png)​

‍

# 6 DC 使用过程

1. 预综合过程(Pre-synthesis Processes)
2. 施加设计约束(Contrainting the Design)
3. 设计综合(Synthesizing the Design)
4. 后综合过程(Post-synthesis Process)

![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%206-20230902111216-ps20xlm.png)

## 6.1 预综合过程

- Design Compiler 的启动
- 设置各种库文件
- 创建启动脚本文件
- 读入设计文件
- DC 中的设计对象
- 各种模块的划分
- Verilog 的编码

### 6.1.1 DC 的启动

![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%207-20230902111216-4utqkrf.png)

`dc_shell-t -f script`​​ 直接调用 tcl 脚本

`command.log` `view_command.log` ：用于记录用户在使用 Design Compiler 时所执行的命令以及设置的参数

`filenames.log` ：退出 design compiler 时会被自动删除
用于记录 design compiler 访问过的目录，包括库、源文件等

启动 dc_shell：只产生 command.log 的日志文件

指定指令后加 `-help` ​可查看帮助，`man` ​查看详细信息

### 6.1.2 读入设计文件

![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%208-20230902111216-d3lw4fl.png)​

![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%209-20230902111216-eavn61q.png)​

* read

  `read -format verilog[db、vhdl etc.] file//dcsh的工作模式`​

  `read_db file.db//TCL 工作模式读取DB格式 `

  `read_verilog file.v //TCL工作模式读取verilog格式 `

  `read_vhdl file.vhd //TCL工作模式读取VHDL格式`​
* analyze & elaborate

  analyze：

  分析 HDL 的源程序并将分析产生的中间文件仔于 work(用户也可以自己指定)的目录下
  进行**语法检查**，产生 `.syn` ​文件，用于 elaborate

  elaborate：

  在产生的中间文件中生成 verilog 的模块或者 VHDL 的实体

- link

  当读取完所要综合的模块之后，需要使用 `link` 命令将读到 Design Compiler 存储区中的模块或实体连接起来

  unresolved design reference 的警告信息:

  需要重新读取该模块
  在 `.synopsys_dc.setup` 文件中添加 `link_library`，告诉 DC 到库中去找这些模块

  同时还要注意 `search_path` 中的路径是否指向该模块或单元电路所在的目录。

## 6.2 设计约束

### 6.2.1 面积约束

`dc_shell-t> current_design PRGRM_CNT_TOP dc_shell-t> set_max_area 100`

上面的例子给 PRGRM_CNT_TOP 的设计施加了一个最大面积 100 单位的约束。100 的具体单位是由 Foundry 规定的，定义这个单位有三种可能的标准:
第一种是将一个二输入与非门的大小作为单位 1;
第二种是以晶体管的数目规定单位;
第三种则是根据实际的面积(平方微米等等)。

可以通过下面的一个小技巧得到：即先综合一个二输入与非门，用 report_area 看他的面积是多少。
如果是 1，则是按照第一种标准定义的;
如果是 4，则是第二种标准;
如果是其他的值，则为第三种标准。

### 6.2.2 时序约束

时序分析STA[^1]

![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226172837-1vnbn1q.png)​

1. 寄存器到寄存器

   `dc_shell-t> create_clock -period 10 [get_ports Clk] `​

   `dc_shell-t> set_dont_touch_network [get_clocks Clk]`​

   clk 时钟树不进行综合，因为位置不固定，实际需要考虑布线后的物理信息
2. 输入到寄存器

   ![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%2010-20230902111216-7kgxyhs.png)

   `dc_shell-t> set_input_delay -max 4 -clock Clk [get_ports A]`​
3. 寄存器到输出

   ![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%2011-20230902111216-64e2634.png)

   `dc_shell-t> set_output_delay -max 5.4 -clock Clk [get_ports B]`​

### 6.2.3 DRC 约束

物理约束

* `set_max_transition`​
  是约束 design 中的信号、端口、net 最大 transition 转换时间不能超过这个值，当然是越小越严苛了。

  net 的 transition time 取决于 net 的负载(fanout)，负载越大，transition time 越大。

  对 clk 的约束比 data 的更严格
* `set_max_fanout`​
  对 design, net, output port 进行操作，设定的不是具体的电容值。

  **扇出负载值**是用来表示**单元输入引脚相对负载的数目**，它并不表示真正的电容负载，而是个无量纲的数字。典型值 32~40
* `set_max_capacitance`​
  最大电容值
  基于工艺库的信息进行设定。

前两个较常见人工设置

## 6.3 环境约束

![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%2012-20230902111216-fb3fmsu.png)

### 6.3.1 工作条件

* PVT：工艺 P；电压 V；温度 T

  工作条件包括三方面的内容:温度、电压以及工艺。

  ![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226184021-ja85m1k.png)​

  这些工作条件一般分为三种:最好情况(best case)、典型情况(typical case)以及最差情况(worst case)。

  一般综合只要考虑到最差和最好两种情况，最差情况用于作基于建立时间(setup time)的时序分析，最好情况用于作基于保持时间(hold time)的时序分析。

* 工艺角（Process Corners）

  把 NMOS 和 PMOS 晶体管的速度波动范围限制在由四个角所确定的矩形内。这四个角分别是：快 NFET 和快 PFET，慢 NFET 和慢 PFET，快 NFET 和慢 PFET，慢 NFET 和快 PFET。例如，具有较薄的栅氧、较低阈值电压的晶体管，就落在快角附近。从晶片中提取与每一个角相对应的器件模型时，片上 NMOS 和 PMOS 的测试结构显示出不同的门延迟，而这些角的实际选取是为了得到可接受的成品率。因此，只有满足这些性能的指标的晶片才认为是合格的。**在各种工艺角和极限温度条件下对电路进行仿真是决定成品率的基础。**

  SS、 TT、FF 分别是左下角的 corner, 中心、右上角 corner

  工艺角分析，corner analysis，一般有五种情况：  
  fast nmos and fast pmos （ff）  
  slow nmos and slow pmos （ss）  
  slow nmos and fast pmos （sf）  
  fast nmos and slow pmos （fs）  
  typical nmos and typical pmos （tt）

  t,代表 typical (平均值) s,代表 slow（电流小） f,代表 fast（电流大）
* PVT

  设计除了要满足上述 5 个 corner 外，还需要满足电压与温度等条件, 形成的组合称为 PVT (process, voltage, temperature) 条件。电压如：1.0v+10% ,1.0v ,1.0v-10% ; 温度如：-40C, 0C 25C, 125C。

  **时序分析中将最好的条件(Best Case)定义为速度最快的情况, 而最坏的条件(Worst Case)则相反**。

  以下列举几种标准 STA 分析条件：

  BCF (Best Case Fast，lt，low-temperature ) : fast process, lowest temperature, high voltage，delay 最小，hold 差  
  TYP (typical) : typical process, nominal temperature, nominal voltage  
  WCS (Worst Case Slow) : slow process, high temperature, lowest voltage  
  WCL (Worst Case & low-temperature) : slow process, lowest temperature, lowest voltage，温度反转效应时 delay 最大，setup 差

  在进行功耗分析时，可能是另些组合如：  
  ML (Maximal Leakage ) : fast process, high temperature, high voltage，温度反转效应下 delay 最小，hold 差。  
  TL (typical Leakage ) : typical process, high temperature, nominal voltage

### 6.3.2 输出负载和输入驱动强度

* 输出负载：

  为了更准确估计模块输出的时序，除了直到输出延时以外，还需要直到输出所接电路的负载情况

  如果输出负载过大会加大电路的 transition time，影响时序特性；由于 DC 默认输出负载为 0 即相当于不接负载的情况，这样综合出来的电路时序显然过于乐观不能反映实际工作情况

  1. 直接设置

  `set_load 5 [get_ports OUT1]`​

  2. 使用工艺库中单元作为负载 Use set_load load_of lib/cell/pin to place the load of a gatefrom the technology library on the port:

  `set_load [load_of my_lib/and2a0/A] [get_ports OUT1]`​

  `set_load [expr [load_of my_lib/invla0/A]* 3] OUT1`​
* 输入电平转换时间：

  `set_driving_cell` ​允许用户可以自行定一个实际的外部驱动 cell:

  -默认情况下，DC 假定外部信号的 transition time 为 0
  -可以让 DC 能够计算一个实际的(non-zero) transition time

  `dc_shell-t> set_driving_cell -lib_cell and2a0 [get_ports IN1]`​

### 6.3.3 连线负载模型

* 模块内连线

  连线延时通过设置**连线负载模型**确定。由 Fab 提供。

  ![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%2013-20230902111216-2pevzqx.png)

  设置输入驱动是通过 DC 的 `set_wire_load_model` 命令完成的。
  Manual model selection:
  `dc_shell-t> set current_design addtwo`
  `dc_shell-t> set_wire_load_model -name 160KGATES`

  ![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%2014-20230902111216-s46bpcd.png)

* 模块间连线：

  **连线负载模式**(set_wire_load_mode)共有 3 种，围绕(enclosed)、顶层(top)以及分段(segmented)。

  `dc_shell-t> set_wire_load_mode enclosed`

  ![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%2015-20230902111216-0liwibh.png)

## 6.4 编译综合

通过 compile 或者 compile_ultra 指令开始后，DC 的工作流程：

1. 简化设计（Simplifying Design 'dut'）

   通过分析逻辑删掉没有用的逻辑器件
2. Mapping 分模块进行映射（Beginning Pass 1 Mapping）

   进行逻辑融合 merge，节省面积；删除常量寄存器

   通过分析逻辑删掉没有用的逻辑器件

   映射 ICG 电路（Mapping integrated clock gating circuitry）
3. Updating timing information

   映射 ICG 电路

   删除无用设计

   计算时序

   更名

   进行逻辑融合 merge，节省面积（识别相同逻辑的信号）；删除常量寄存器
4. Beginning Mapping Optimizations  (Ultra High effort)

   add designware key list to design

   删除常量寄存器
5. 。。。

## 6.4 约束检查

在定义完环境属性之后,可以使用下面的几个命令检查约束是否施加成功。

`check_timing`,检查设计是否有路径没有加入约束。

`check_design`​，检查设计中是否有悬空管脚、多驱动或者输出短接的情况。

`write_script`，将施加的约束和属性写出到一个文件中，可以检查这个文件看看是否正确，综合阶段的脚本很重要，后续的流程都需要以这个脚本为时序基准。

## 6.5 结果记录

### 6.5.1 保存结果

逻辑综合后，需要保存的文件有:

1. 我们要把整个的工程以 db 的格式保存下来，方便后续的查询。
2. 我们要保存网表,这个网表我们要用于后面布局布线和后仿真。
3. 需要保存 sdf 文件, sdf 里标注了标准单元的延迟值，我们在后仿真的时候需要用到 sdf 文件。

```tcl
# 保存文件,对应图形界面的7
##########################
# write *.db and *.v #
##########################
# write -f db -hier -output ./out/nor2.db
write -f verilog -hier -output ./out/nor2netlist.v
# 保存反标文件
write_sdf -version 2.1 ./out/nor2.sdf 
```

### 6.5.2 保存报告

除了这些文件之外，我们还要保存产生的各种报告。报告重点包括面积报告、时序报告和约束违例报告。重点关注时序报告的内容。

```tcl
# 产生报告并保存,对应图形界面的6
##########################
# generate reports #
##########################
# 把报告面积的文件保存成EXAMPLE1.area_rpt文件,运行完脚本以后可以查看该文件。
report_area > ./rpt/nor2.area_rpt 
report_constraint -all_violators > ./rpt/nor2.constraint_rpt
report_timing > ./rpt/nor2.timing_rpt
sh date # 显示结束时间
```

### 6.5.3 时序报告

时序报告的主要内容:  表头 - 数据发射路径 - 数据捕获路径 - 时序结果

- 表头

  ![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%2016-20230902111216-qp4166b.png)
- 数据发射路径

  ![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%2017-20230902111216-f5eirxk.png)
- 数据捕获路径

  ![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%2018-20230902111216-kqh1uhp.png)
- 时序结果

  ![Untitled](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/Untitled%2019-20230902111216-372eiy4.png)

## 6.6 DC 脚本举例

```tcl
# run_nor2.tcl

sh date # 显示开始时间
# 移除DC中原有的设计
remove_design -designs
# 下面是库的设置,对应图形界面操作的2
##########################
# set library #
##########################
set synthetic_libraray [list /data/eda/synopsys/syn2019/libraries/syn/dw_foundation.sldb]
set search_path [list /home/jiexxpu2/ncnnAccel/smic0.18_lib/FEView_STDIO/Version2.4/STD/Synopsys /home/jiexxpu2/ncnnAccel/accel]
set target_library { smic18_tt.db }
set link_library {*}
# set symbol_library { tt.sdb }

# 下面是屏蔽一些warning信息,DC在综合时遇到这些warning时就把它们忽略,不会报告这些信息,VER-130,VER-129等是不同warning信息的编码,具体含义可以查看帮助
##########################
#void warning Info#
##########################
suppress_message VER-130
suppress_message VER-129
suppress_message VER-318
suppress_message ELAB-311
suppress_message VER-936

# 读入example1.v文件,对应于图形界面的3
################################
#read&amp;link&amp;Check design#
################################
read_file -format verilog nor.v

#analyze -format verilog ~/example1.v
#elaborate EXAMPLE1

# 把EXAMPLE1指定为当前设计的顶层模块
current_design nor2 
uniquify
check_design

# 设置一些变量
#############################
#define IO port name#
#############################
# 设置变量clk的值是[get_ports clk],在下面的代码中若出现$clk字样,则表示引用该变量的值,即用[get_ports clk]代替$clk。
set clk [get_ports clk]
set rst_n [get_ports rst_n]

set general_inputs [list A B]
set outputs [get_ports C]

# 设置约束条件,对应于图形界面的4
#############################
#set_constraints#
#############################
# 设置时钟约束,对应于图形界面的4.1
#1 set constraints for clock signals
# 创建一个周期为20ns,占空比为1的时钟
create_clock -n clock $clk -period 20 -waveform {0 10}
set_dont_touch_network [get_clocks clock]
# 设置时钟端口的驱动为无穷大
set_drive 0 $clk 
# 设置时钟端为理想网络
set_ideal_network [get_ports clk] 

# 设置复位信号约束,对应于图形界面的4.2
#2 set constraints for reset signals
set_dont_touch_network $rst_n
set_drive 0 $rst_n
set_ideal_network [get_ports rst_n]

# 设置输入延时,对应图形界面的4.3
#3 set input delay
set_input_delay -clock clock 8 $general_inputs
# 设置输出延时,对应图形界面的4.4
#4 set output delay
set_output_delay -clock clock 8 $outputs

# 设置面积约束和设计约束,对应图形界面的4.5
#5 set design rule constraints
set_max_fanout 4 $general_inputs
set_max_transition 0.5 [get_designs "nor2"]
#6 set area constraint
set_max_area 0

# 综合优化,对应图形界面的5
#############################
#compile_design#
#############################
compile -map_effort medium

# 保存文件,对应图形界面的7
##########################
# write *.db and *.v #
##########################
# write -f db -hier -output ./out/nor2.db
write -f verilog -hier -output ./out/nor2netlist.v
# 保存反标文件
write_sdf -version 2.1 ./out/nor2.sdf 

# 产生报告并保存,对应图形界面的6
##########################
# generate reports #
##########################
# 把报告面积的文件保存成EXAMPLE1.area_rpt文件,运行完脚本以后可以查看该文件。
report_area > ./rpt/nor2.area_rpt 
report_constraint -all_violators > ./rpt/nor2.constraint_rpt
report_timing > ./rpt/nor2.timing_rpt
sh date # 显示结束时间

quit
```

# 7 优化电路 Optimization

DC 进行优化的目的是权衡 timing 和 area 约束，以满足用户对功能，速度和面积的要求。 优化过程是基于用户为 design 所加载的约束。而约束分为两种，一种为 design rule constraint （主要包括 transition， fanout， capacitance 和 cell degradation），一种为 optimization constraint（包括 delay 和 area）。 DC 默认 DRC 约束有较高优先权，必须先于 optimization 约束满足。

![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226185355-mp30fn8.png)​

[芯动力——硬件加速设计方法_西南交通大学](https://www.icourse163.org/learn/SWJTU-1207492806?tid=1467145670#/learn/content?type=detail&id=1248235663&cid=1274910513)

## 优化方法

1. path group

   DC 根据不同的时钟划分 path group。但是如果设计存在复杂的时钟，复杂的时序要求或者复杂的约束，用户可以将所关心的几条关键路径划分为一个 path group，指定 DC 专注于该组路径的优化。

   ![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226185656-65cgw88.png)​

   默认情况下， DC 只优化关键路径。 通常来说，电路的如果在关键路径可能会存在成千上万条，因此只优化一条其实并没有太大实质性的意义。如果我们在关键路径附近指定一个范围，那么 DC 就会优化指定范围之内的所有路径。 但是， 若指定范围较大，会增大 DC 运行时间，因此一般情况该范围设定为时钟周期的 10%。

   ![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226185839-afj38f0.png)

   ![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226190243-xfzr6k0.png)​​
2. High-effort compile

   High-effort compile 能够是 DC 更加努力地达到所约束的目标，该措施在关键路径上进行重新综合，同时对关键路径周围的逻辑进行了 restructure 和 remap。

   1. compile_ultra command：附带两个 option，这两个 option 分别包含一些的脚本，提供额外的时序和面积优化
      option 为 `-area_high_effort_script option`​ 和 `-timing_high_effort_script`​。

   2. compile command：附带一个 option，`map_effort -high`​

   High-effort 对关键路径的重新优化包括: 逻辑复制和映射为大扇入的门单元( Logic Duplication and Mapping to Wide-Fanin Gates )

   如果用一些工艺库中结构复杂、扇入较大的门单元代替关键路径上的一些标准单元，能够获得更好的电路的时序和面积，DC 会在关键路径上做相应的优化
   此外，对于一些高扇出或者高负载的线，DC 会通过逻辑复制和重新构造(restructuring)这部分电路来进行优化。这些逻辑复制和重新构造通常会导致设计在面积上有显著的增加。
3. incremental

   通常，使用 **incremental** 可以提高电路优化的性能。如果电路在 compile 之后不满足约束，通过 incremental 也许能够达到要求的结果。

   Incremental 只进行门级（gate-level）的优化，而不是逻辑功能级（logic-level）的优化。它的优化结果可能是电路的性能和之前一样， 或者更好。 Incremental 会导致大量的计算时间，但是对于将最差的负 slack 减为 0，这是最有效的办法。

   为了减少 DC 运算时间，可将那些已经满足时序要求的模块设置为 dont_touch 属性。 对于那些有很多违例逻辑模块的设计，通常 Incremental 最有效。

   dc shell> dont_touch noncritical_blocks
   dc shell> compile -map_effort high -incremental_mapping

   门级优化主要通过选择库中的合适的标准单元来对电路进行优化，它分为三个阶段执行： delay optimization, design rule fixing 和 area recovery。

   1. 在 delay optimization 阶段， DC 对电路进行局部的调整。在该阶段， DC 已经开始考虑 DRC，同样条件下，会选择 DRC 代价最小 的方案。
   2. 在 design rule fixing 阶段， DC 主要通过插入 buffer，调整单元的大小等措施来满 足各种 DRC 的约束。这个阶段一般不会影响时序和面积结果，但是会引起 optimization constraints 违例。
   3. area recovery 阶段，不会引起 DRC 和 delay 的违例，一般只是对非关键路径进行优化。如果没有设置面积约束，那么优化的幅度会很小。
4. ungrouping

   DC 还可以通过优化代码层次关系来对电路优化。 **Ungrouping** 可以取消设计中的层次，移除层次的边界，并且允许 DC Ultra 通过减少逻辑级数改进时序，以及通过共享资源减小面积。

   具体使用过程中， 可通过使用 `compile_ultra`​ 命令，或者 `compile -auto_ungroup`​ 来实现该功能。

   其中 compile_ultra 提供两种策略： `delay-based auto-ungrouping`​ 和 `areabased auto-ungrouping`​。compile_ultra 也是一个高级功能，需要单独的 license 才可以使用

   ![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226191637-yaeks3s.png)​

   Commands:

   `ungroup -flatten -all`​​
   ​`compile -ungroup`​​
5. retiming

   如果在逻辑综合过程中，如果发现设计的流水线划分不平衡，就可以使用 **retiming** 策略， 可在时序路径上前后移动寄存器，以提高电路的时序性能。 在前后移动过程中，除了对时序有影响，对电路的面积也会有一定影响。通常会放在逻辑块的入口和出口，让 dc 自动优化。

   ![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226191742-39jkrh0.png)

   ![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226191816-wq1yaiu.png)​​
6. 数据通路

   DC 还可以专门对**数据通路**进行优化，通常主要通过以下手段：

   1. 采用树形结构的运算逻辑，比如这个示例中，可以将加法器减少。

      ![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226191846-32tusak.png)​
   2. 逻辑上的简化，比如： (a\*3\*5)简化为(a*15)，减少 1 个乘法器。
   3. 资源共享。 其中， 如图所示， 资源共享涉及到一个经常会遇到的问题：先选后加，还是先加后选。要注意其中的差别。

      ![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226191912-5kevlrc.png)​
7. DC Ultra

   **DC Ultra** 对数据的优化主要通过以下手段：

   1. 使用 design ware 库。design ware 可以提供更高性能的运算器。
   2. 数据路径提取：使用多个树形阵列的 CSA 的加法器代替数据通路中的加法运算，可大大提高电路的运算速度。但是，这只适合多个运算单元之间没有任何逻辑。同时， design ware 中的单元不能被提取。
   3. 对加乘进行重新分配，比如：\(A\*C+B\*C\)优化为(A+B)*C，减少运算器的个数。
   4. 比较器共享，比如 A>B, A<B, A<=B 会调用同一个减法器。
   5. 优化并行的常数相乘。
   6. 操作数重排。
8. Flattening

   Flattening（展平）常常被误认为是删除层次结构并使设计平面化。这不是真的。Flattening 实际上是降低逻辑电平和提高速度的优化技术设计。它的工作原理是将组合逻辑转换为两级乘积之和形成并去除中间项。Flattening 不是默认情况下执行。

   Example:

   `A = b + c;`​​
   ​`out = A * d;`​​

   After flattening is enabled, the equation is

   `out = (b + c) * d; => b*d + c*d;`​​

   Command:

   `set flatten true`​​
9. Structuring

   Structuring（结构化）为设计添加了中间变量和逻辑结构。它用于减少设计面积。在结构化过程中，DC 搜索可以被分解的子函数，并评估每一个因子，根据因子的大小以及因子出现的次数，工具将最简化逻辑的子函数转换为中间变量，并将它们从设计方程中剔除。

   Example:

   `y1 = ab + ac`​​
   ​`y2 = b + c + d`​​

   This requires four logic gates (two AND and two OR gates).

   After structuring the equations can be written as

   `y1 = aT`​​
   ​`y2 = T + d`​​

   where `T = b + c`​​

   The structured logic requires only three gates.

   Structuring 分为 timing driven 以及 Boolean。Timing-driven structuring 默认开启，只影响关键路径；Boolean 的则不默认开启，他会减少面积，对于一些 area-critical designs，不确定能否提高时序。

   Command:

   `set_structure`​​

## 注意事项

* 等价性

  以下优化会引起网表和 RTL design 不一致，因此需要使用 formality 工具进行一致性检查，确认不一致的地方是否由 DC 优化造成：

  1. 由 ungroup, group, uniquify, rename_design 等造成部分寄存器，端口名字改变。
  2. 等效和相反的寄存器被优化，常量寄存器被优化。
  3. Retiming 策略引起的寄存器，电路结构不一致。
  4. 数据通路优化引起的不一致。
  5. 状态机的优化。

  因此， DC 在综合过程中必须生成 formality 的 setup 文件（默认为 default.svf）。 formality 使用的时候，也应该去读取该 svf 文件。

  svf 文件是 DC 综合过程中产生的文件，用来记录 DC 对网表产生的一些变化，防止后续的 rtl 和门级网表对应不上的问题。
* 模块划分原则

  在运用 DC 作综合的过程中， 默认的情况下各个模块的层次关系是保留着的，保留着的层次关系会对 DC 综合造成一定的影响，比如在优化的过程中，各个子模块的管脚必须保留，这势必影响到子模块边界的优化效果。 那么模块应该如何划分，才能够得到一个好的综合结果呢？

  1. 不要让一个组合电路穿越过多的模块。

     ![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226192221-oqf1597.png)​![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226192257-35pmhw9.png)​![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226192313-897rf8v.png)​

     ![image](https://gitee.com/chenshengjie1999/notebook/raw/master/Synthesis/assets/image-20231226192351-u6hchb3.png)​​​​

  2. 寄存模块的输出。

  3. 根据综合时间长短控制模块大小。

  4. 将同步逻辑部分与其他部分分离。

# 8 Synopsys TCL

object 是对于 tcl 脚本一个重要的扩展;常见的对象有四种 cell, net, port, pin

查看 design 中有多少个 net：

使用 TCL 自建指令：`llength [get_object_name [get_nets *]]` 先通过 get_nets *获取包含所有 net 的 collection，然后通过 get_object_name 的指令得到所有的名称的列表

使用 Synopsys TCL：`sizeof_collection [get_nets *]`

pin :

`get_pins */Z`

`get_pins */Q*`

port:

`get_ports CLK`

`get_ports *`

cell:

`get_cells U4`

每种 object 有它的**属性**。任何一个属性都可以用 `get_attribute` 得到, `list_attribute -class *` 可以得到所有 object 的属性，部分属性可以用 `set_attribute` 来设置。

- Cell object
  属性 `ref_name`:用来保存其 map 到的 reference cell 名称 `Shell> get_attribute [get_cells -h U3] ref_name # {INV}`
- Pin object:
  属性 `owner_net`:用来保存与之相连的 net 的名称 `Shell> get_attribute [get_pins U2/A] owner_net # {BUSO}`
- Port object:
  属性 `direction`:用来保存 port 的方向
  `Shell> get_attribute [get_ports A] direction # {in}`
  `shell> get_attribute [get_ports OUT[1]] direction # {out}`
- Net object:
  属性 `full_name`:用来保存 net 的名称
  `Shell> get_attribute [get_nets INVO] full_name # {INVO}`
  `Shell> get_object_name [get_nets INVo] # {INVO}`

  > *Shell> get_attribute INVO full_name  ## Error: No attribute found
  >

`get_* -f` 指令，`-f` 这个 option 可以用来过滤属性，以得到我们想要的 object。例如，

想得到所有方向是 input 的 port，使用 `get_ports * -f “direction==in"`，可以返回所有方向是 input 的 port。

想得到所有方向是 output 的 pin，使用 `get_pins * -f "direction ==out"`，也可以得到结果。

想得到所有 ref_name 是 INV 的 cell，使用 `get_cells * -f “ref_name == INV"` 即可返回。

`get_* [object] -of`。`-of` 这个 option 可以用来得到与你指定 object 相连接的 object。例如，

想知道 port 和 net 的互连关系，可以使用 `get_nets -of [get_port A]`。

如果想知道 pin 和 net 的互连，可以使用 `get_nets -of [get_pin u2/A]`。

如果想知道 pin 和 net 的互连, `get_pins -of [get_net lNV1]`。

如果想知道 cell 和 pin 的互连，可以使用 `get_pins -of [get_cell U4]`。

‍
